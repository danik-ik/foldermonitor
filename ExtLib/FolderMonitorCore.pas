(******************************************************************************
  �������� (� ��������� ������) ��������� � �����, ������������
  ���������������� �������.

  �� ��������� ������ ���������� �������:
  https://webdelphi.ru/2011/08/monitoring-izmenenij-v-direktoriyax-i-fajlax-sredstvami-delphi-chast-1/

  ����������� �������:
  - ��������� ����� ����� ��� �����
  - ��������� �������
  - ��������� ������� ��������� ������
 ******************************************************************************)
unit FolderMonitorCore;

interface

uses Classes, Windows, SysUtils;

type
  TFolderMonitorCore = class(TThread)
    private
      FDirectory: string;
      FScanSubDirs: boolean;
      FOnChange   : TNotifyEvent;
      procedure DoChange;
    public
      constructor Create(ASuspended: boolean; ADirectory:string; AScanSubDirs: boolean);
      property OnChange: TNotifyEvent read FOnChange write FOnChange;
    protected
      procedure Execute; override;
  end;

implementation

{ TFolderMonitorCore }

constructor TFolderMonitorCore.Create(ASuspended: boolean; ADirectory: string;
  AScanSubDirs: boolean);
begin
  inherited Create(ASuspended);
  FDirectory:=ADirectory;
  FScanSubDirs:=AScanSubDirs;
  FreeOnTerminate:=true;
end;

procedure TFolderMonitorCore.DoChange;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TFolderMonitorCore.Execute;
var ChangeHandle: THandle;
begin
  // ���������� �������� ���������, �������� ��������������� �����
  ChangeHandle:=FindFirstChangeNotification(PChar(FDirectory),
                                            FScanSubDirs,
                                            FILE_NOTIFY_CHANGE_FILE_NAME+
                                            FILE_NOTIFY_CHANGE_DIR_NAME+
                                            FILE_NOTIFY_CHANGE_SIZE+
                                            FILE_NOTIFY_CHANGE_LAST_WRITE
                                            );
  // ��������� ������������ ���������, ����� ������������� ����������
{$WARNINGS OFF}
  Win32Check(ChangeHandle <> INVALID_HANDLE_VALUE);
{$WARNINGS ON}
  try
    // ��������� ���� ���� �� ������� ������ �� ����������
    while not Terminated do
    begin
      { ������ ������� �� ������������� �������: �������� �� ����������,
        ������������ ����������� ���� ������ }
      case WaitForSingleObject(ChangeHandle, 1000) of
        WAIT_FAILED: Terminate; {������, ��������� �����}
        WAIT_OBJECT_0: // ��������� ���������
          begin
            // �������� ����������� �� ��������� ������� �� ��������� ��������� ������
            // � �������� ������������� ����������
            // (��� ���������� ��������� � Sublime Text ����������� ���������).
            // �������� ���� ���������� ��������� �������: ���� ��������� ����������
            // ����� ������� ����������, �� �� ��������� ���������� ����������� ��������.
            // �������� �������� ��������� ������� � ����.
            sleep(5);
            WaitForSingleObject(ChangeHandle, 1); // ��������� ��� ����, ������� ��������� �� �����������
            // ��� ���� �������: ���������� ����� ��������� ����� �������� ���������
            if not Terminated then
              Synchronize(DoChange);
          end;
        WAIT_TIMEOUT: {DO NOTHING}; // ��� �� ��������� ����,
                                    // ���� ��������� �� ������� ����� 
      end;
      FindNextChangeNotification(ChangeHandle);
    end;
  finally
    FindCloseChangeNotification(ChangeHandle);
  end;
end;

end.
