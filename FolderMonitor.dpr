program FolderMonitor;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  FolderMonitorCore in 'ExtLib\FolderMonitorCore.pas',
  ConsoleRunner in 'ExtLib\ConsoleRunner\ConsoleRunner.pas',
  FolderMonitorApp in 'FolderMonitorApp.pas',
  ConsoleKbd in 'ConsoleKbd.pas',
  ConsoleFolderMonitorApp in 'ConsoleFolderMonitorApp.pas';

var
  Folder: string;
  Command: string;

function UseParameters(): boolean;
var
  i: integer;
begin
  Result := false;
  if (ParamCount < 2) then
  begin
    Writeln('usage: FolderMonitor <Tracked folder> <command to run on folder change>');
    exit;
  end;
  Folder := ParamStr(1);
  Command := ParamStr(2);
  for i := 3 to ParamCount do
    Command := Command + ' ' + ParamStr(i);
  if (not DirectoryExists(Folder)) then
  begin
    Writeln(Format('F�lder "%s" is not exists', [Folder]));
    exit;
  end;
  Result := true;
end;

begin
  if not UseParameters then
    exit;
  with TConsoleFolderMonitorApp.Create(Folder, Command) do
  try
    execute;
  finally
    free;
  end;
end.
