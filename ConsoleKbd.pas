unit ConsoleKbd;

Interface

function KeyPressed: Boolean;
function ReadKey: char;

implementation

uses Windows;

var
  hConsoleInput: THandle;
function KeyPressed: Boolean;
var
  NumberOfEvents: DWORD;
  NumRead: DWORD;
  InputRec: TInputRecord;
  Pressed: boolean;
begin
  Pressed := False;
  GetNumberOfConsoleInputEvents(hConsoleInput, NumberOfEvents);
  if NumberOfEvents > 0 then
  begin
    if PeekConsoleInput(hConsoleInput, InputRec, 1,NumRead) then
    begin
      if (InputRec.EventType = KEY_EVENT)
      and (InputRec.Event.KeyEvent.bKeyDown) then
        Pressed := True
      else
        ReadConsoleInput(hConsoleInput, InputRec, 1,NumRead);
    end;
  end;
  Result := Pressed;
end;

function ReadKey: char;
var
  NumRead: DWORD;
  InputRec: TInputRecord;
begin
  repeat
  until KeyPressed;
  ReadConsoleInput(hConsoleInput, InputRec, 1,NumRead);
  Result := InputRec.Event.KeyEvent.AsciiChar;
end;


initialization
  hConsoleInput := GetStdHandle(STD_INPUT_HANDLE);
end.

