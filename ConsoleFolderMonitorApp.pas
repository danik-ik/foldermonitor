unit ConsoleFolderMonitorApp;

interface
uses
  FolderMonitorApp;

type
  TConsoleFolderMonitorApp = class(TFolderMonitorApp)
    procedure ShowStart; override;
    procedure ShowStop; override;
    procedure callCommandAndWait; override;
    function getControlEvent: TFolderMonitorControlEvent; override;
    procedure CheckSynchronize; override;
  end;

implementation
uses
  ConsoleRunner, ConsoleKbd, Classes, Messages, SysUtils;

{ TConsoleFolderMonitorApp }

procedure TConsoleFolderMonitorApp.callCommandAndWait;
begin
  ExecConsoleAppInteractively(Command);
  if self.isRunning then
    Writeln('[Folder monitoring] Waiting for changes...')
  else
    ShowStop;
end;

procedure TConsoleFolderMonitorApp.CheckSynchronize;
begin
  classes.CheckSynchronize();
end;

function TConsoleFolderMonitorApp.getControlEvent: TFolderMonitorControlEvent;
var
  ch: char;
begin
  Result := fmceNothing;
  while keypressed do
  begin
    ch := ReadKey;
    case ch of
    'p', 'P', ' ':
      if IsRunning then
        Result := fmceStop
      else
        Result := fmceStart;
    #13:
      Result := fmceForce;
    'q', 'Q':
      Result := fmceExit;
    end;
  end;
end;

procedure TConsoleFolderMonitorApp.ShowStart;
begin
  Writeln(Format('[Folder monitoring] STARTED for "%s"', [self.Folder]));
  Writeln('Control: p or space for pause, Enter to force start, Ctrl+C or q for exit');
end;

procedure TConsoleFolderMonitorApp.ShowStop;
begin
  Writeln('[Folder monitoring] PAUSED');
end;

end.
