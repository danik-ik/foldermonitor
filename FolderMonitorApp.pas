unit FolderMonitorApp;

interface
uses
  FolderMonitorCore, SysUtils, Windows;

type
  TFolderMonitorControlEvent = (fmceNothing, fmceStart, fmceStop, fmceForce, fmceExit);

  TFolderMonitorApp = class
  protected
    Command: string;
    Folder: string;
    isRunning: boolean;
    procedure ShowStart; virtual; abstract;
    procedure ShowStop; virtual; abstract;
    procedure callCommandAndWait; virtual; abstract;
    function getControlEvent: TFolderMonitorControlEvent; virtual; abstract;
    procedure CheckSynchronize; virtual; abstract;
  private
    monitor: TFolderMonitorCore;
    wasChanged: boolean;
    terminated: boolean;
    procedure start;
    procedure stop;
    procedure onChange(sender: TObject);
    procedure reset;
  public
    constructor Create(Folder, Command: string);
    destructor destroy; override;
    procedure Execute;
  end;

implementation

{ TFolderMonitorApp }

constructor TFolderMonitorApp.Create(Folder, Command: string);
begin
  self.Folder := ExpandFileName(Folder);
  self.Command := Command;
end;

destructor TFolderMonitorApp.destroy;
begin
  stop;
  inherited;
end;

procedure TFolderMonitorApp.Execute;
begin
  start;
  Repeat
    case self.getControlEvent of
    fmceNothing: ;
    fmceStart: start;
    fmceStop: stop;
    fmceForce: callCommandAndWait;
    fmceExit: terminated := true;
    end;
    CheckSynchronize; // ��������� syncronized �������
    if wasChanged then
    begin
      reset;
      callCommandAndWait;
    end;
  until terminated;
  stop;
end;

procedure TFolderMonitorApp.onChange(sender: TObject);
begin
  wasChanged := true;
end;

procedure TFolderMonitorApp.reset;
begin
  wasChanged := false;
end;

procedure TFolderMonitorApp.start;
begin
  isRunning := true;
  ShowStart;
  callCommandAndWait;
  monitor:=TFolderMonitorCore.Create(True, Folder, True);
  monitor.OnChange := OnChange;
  monitor.Resume;
end;

procedure TFolderMonitorApp.stop;
begin
  isRunning := false;
  ShowStop;
  if Assigned(monitor) then
    monitor.Terminate;
  monitor := nil; // ���������� �� �������������� �� ����������
end;

end.
